#include <iostream>
#include "queue.h"

/*
	This function will initiate the queue with the given size
	input: The pointer to the queue we want to intitiate and the size that we need to make the queue with
	output: None (void)
*/
void initQueue(queue* q, unsigned int size)
{
	int i = 0;

	q->_elements = new unsigned int[size];
	q->_maxSize = size;
	q->_count = 0; //This means that we currently have 0 items

	/* Making sure we don't have any trash values (putting each item to 0 for default value) */
	for (i = 0; i < q->_maxSize; ++i)
	{
		q->_elements[i] = 0; 
	}
}

/*
	This function will clean the Queue. Restting the count and resetting all the elements.
	input: A pointer to the queue
	output: None (void)
*/
void cleanQueue(queue* q)
{
	delete[] q->_elements;

	/* Restting the count as we don't have any elements now */
	q->_count = 0;
}

/*
	This function will add an item to the queue.
	input: The pointer to the queue, the new value that we want to add
	output: None (void)
*/
void enqueue(queue* q, unsigned int newValue)
{
	//Only add a new item if the count is lower then the _maxSize because if it's not then we're out of places to put items in.
	if (q->_count < q->_maxSize)
	{
		//Add the new value in the place of the count (The place of the current index)
		q->_elements[q->_count] = newValue;

		//Add +1 to the count so next time we add something it will point to the next place.
		++q->_count;
	}
	else
	{
		std::cout << "The Queue is full!" << std::endl;
	}
} 

/*
	This function will return the queued item to the user or -1 if there are not items
	input: The pointer to the queue
	output: either the queued item or -1 (Telling the user there is no item as the queue can only contain postive numbers)
*/
int dequeue(queue* q)
{
	int item = 0;

	/* We can only get an item if the count is larger then 0 beacuse only then we have items in the queue.*/
	if (q->_count > 0)
	{
		/* Giving back the item at the place of (maxSize - count) */
		item = q->_elements[q->_maxSize - q->_count];

		/* Deleting the item because we no longer need it */
		q->_elements[q->_maxSize - q->_count] = 0; 

		--q->_count;
	}
	else
	{
		/* If we don't have anything to return then we tell the user and then return NO_ITEM (-1) as the queue can only have unsigned ints*/
		std::cout << "The queue is empty! Please add an item to the queue!" << std::endl;
		item = NO_ITEM;
	}

	return item;
}