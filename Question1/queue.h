#ifndef QUEUE_H
#define QUEUE_H

/* In case there is no item and the user is trying to take an item */
#define NO_ITEM -1

/* a queue contains positive integer values. */
typedef struct queue
{
	/* They are all unsigned because they can't be less then 0*/
	unsigned int* _elements;
	unsigned int _maxSize;
	unsigned int _count; 

} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */