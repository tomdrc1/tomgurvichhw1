#include <iostream>
#include "stack.h"
#include "utils.h"

/*
	This function will get an array and it's size and will reverse it
	input: the array num, the array size
	output: None (void)
*/
void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* s = new stack;

	initStack(s);

	for (i = 0; i < size; ++i)
	{
		push(s, nums[i]);
	}

	for (i = 0; i < size; ++i)
	{
		nums[i] = pop(s);
	}

	cleanStack(s);
	delete s;
}

/*
	This function will get 10 numbers and return an array in the reverse order the user entered them
	output: The array with the numbers in the reversed order they were entered with
*/
int* reverse10()
{
	int i = 0;
	int* arr = new int[ARR_SIZE];
	stack* s = new stack;

	for (i = 0; i < ARR_SIZE; ++i)
	{
		std::cout << "Enter value " << i + 1 << std::endl;
 		std::cin >> arr[i];
	}

	initStack(s);

	for (i = 0; i < ARR_SIZE; ++i)
	{
		push(s, arr[i]);
	}

	for (i = 0; i < ARR_SIZE; ++i)
	{
		arr[i] = pop(s);
	}

	cleanStack(s);
	delete s;

	return arr;
}