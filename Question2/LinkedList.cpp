#include "LinkedList.h"
#include <iostream>

/*
	This function will insert an item to the top.
	input: a pointer to the head poitner (So the changed here will affect main), the item we want to insert to the new node.
	output: None (void)
*/
void add(Node** head, int item)
{
	Node* top = *(head);
	Node* nodeToAdd = createNewNode(item);
	
	/* Connecting the new node to be the new head */
	nodeToAdd->_next = top;
	/* Telling the head that he is now at the new node */
	*(head) = nodeToAdd;
}

/*
	This function will remove the item at the head and will put the head on the next item.
	input: A pointer to the head pointer so the changes here will save to the main
	output: None (void)
*/
void remove(Node** head)
{
	Node* top = *(head);

	if (top)
	{
		*(head) = (*(head))->_next;

		delete top;
	}
}

/*
	This function will create a new node with the item that we give to it.
	input: item which is the item we want to enter the new node
	output: A node pointer (The new node)
*/
Node* createNewNode(int item)
{
	Node* newNode = new Node;

	newNode->_element = item;
	newNode->_next = NULL;

	return newNode;
}