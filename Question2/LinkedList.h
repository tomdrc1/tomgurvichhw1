#ifndef LINKED_LIST
#define LINKED_LIST
	
typedef struct Node
{
	unsigned int _element;
	Node* _next;
} Node;

void add(Node** head, int item);
Node* createNewNode(int item);
void remove(Node** head);

#endif