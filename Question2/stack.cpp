#include "stack.h"
#include "LinkedList.h"
#include <iostream>

/*
	This function will push the element into the top of the stack
	input: a stack pointer, the element we want to insert
	output: None (void)
*/
void push(stack* s, unsigned int element)
{
	Node* temp = createNewNode(element);

	// if there is a stack
	if (s)
	{
		temp->_next = s->_stack;
		s->_stack = temp;
	}
	else
	{
		std::cout << "first make a stack!" << std::endl;
	}
}

/*
	This function will remove the top item and return it's value
	input: A stack pointer
	output: The value of the item that was removed or -1 if the stack is empty.
*/
int pop(stack* s)
{
	int popedItem = -1;
	Node* temp = NULL;

	/* if there is a stack and it's not empty. */
	if (s && s->_stack)
	{
		popedItem = s->_stack->_element;
		temp = s->_stack;
		s->_stack = s->_stack->_next;

		delete temp;
	}

	return popedItem;
}

/*
	This function will init the stack
	input: A pointer to the stack
	output: None (void)
*/
void initStack(stack* s)
{
	if (s)
	{
		s->_stack = NULL;
	}
}

/*
	This function will clean the stack
	input: A pointer to the stack
	output: None (void)
*/
void cleanStack(stack* s)
{
	Node* temp = NULL;

	if (s)
	{
		while (s->_stack)
		{
			temp = s->_stack;
			s->_stack = s->_stack->_next;
			delete temp;
		}
	}
}